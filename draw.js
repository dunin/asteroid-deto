/// CLIENT VARIABLES

const momentumMenu = 10; // time until buttons are active
const c = document.getElementById("canvas");
c.width = width;
c.height = height;
const ctx = c.getContext("2d");

ctx.textAlign = "center";
ctx.font = "72px undefined";

const shakeOffsetMax = 15;
let offsetShakeX =0;
let offsetShakeY =0;

const messageFrame= {x:30,y:200,w:540,h:200};
var frames = [];
var txts = [];


var backgroundColor = "#000";
var lineColor = "#fff";

var player;
var shipImg;

var keys={};
var currentFps;

var loop;
var menuId;

var cielImg;
var tutoState=0;


/// UTILITIES

var fps = {	startTime : 0,
	frameNumber : 0, 
	getFPS : function(){
		this.frameNumber++;	
		var d = new Date().getTime(),
		currentTime = ( d - this.startTime ) / 1000,
		result = Math.floor( ( this.frameNumber / currentTime ) );
		if( currentTime > 1 ){
 			this.startTime = new Date().getTime();
		this.frameNumber = 0;		}
		return result;	}
	};

	function noGame() {
		asts = [];
		ships = [];
		esth = [];
		bulls =[];
		esth = [];
	}


/// DRAW PART

function draw() {
	ctx.setTransform(1, 0, 0, 1, 0, 0);
	ctx.fillStyle=backgroundColor;
	ctx.fillRect(0,0,width,height);

	if(cielImg) {
		ctx.globalAlpha = 0.2;
		ctx.drawImage(cielImg,0,0,width,height);
		ctx.globalAlpha = 1;
	}

	if(counts.shake>0)  {
		offsetShakeX = rand(-shakeOffsetMax,shakeOffsetMax);
		offsetShakeY = rand(-shakeOffsetMax,shakeOffsetMax);
		ctx.translate(offsetShakeX,offsetShakeY);
	}

	ctx.strokeStyle=lineColor;
	ctx.fillStyle=lineColor;

	if(planete) drawThing(planete);

	asts.forEach(a=>{
		drawThing(a);
	});

	if (player.alive) {
	
		drawThing(player);

		// if(player.counts.teleport>0) {
		// 	var start = getPoint(player.dir,rShip*2.5);
		// 	ctx.moveTo(player.x + start.x, player.y + start.y);
		// 	ctx.arc(player.x, player.y, rShip*2.5,player.dir, player.dir+player.counts.teleport/momentumTeleport*Math.PI*2);
		// }
		// ctx.stroke();
	}


	ctx.beginPath();
	bulls.forEach((b)=>{
		ctx.moveTo(b.x,b.y);
		ctx.arc(b.x, b.y, rBullet,0, Math.PI*2);
	});
	ctx.fill();

	// esthetique things;
	esth.forEach(a=>{
		if(a.dx) moveObj(a);
		drawThing(a);
	});

	// ctx.fillStyle=backgroundColor;
	// frames.forEach(f=>{
	// 	ctx.fillRect(f.x,f.y,f.w,f.h);
	// 	ctx.strokeRect(f.x,f.y,f.w,f.h);
	// })

	ctx.fillStyle=lineColor;
	txts.forEach(a=>{
		ctx.fillText(a.txt,a.x,a.y);
		// ctx.strokeText(a.txt,a.x,a.y);
	});

	if (startTime) {
		var elapsed = Math.floor((Date.now() - startTime)/1000);
		var secondes = (elapsed%60)+"";
		if(secondes.length==1) secondes = "0"+secondes;
		ctx.fillText(Math.floor(elapsed/60)+":"+secondes, width-72*4, 72);
	}
}

function drawThing(thing) {
	/*if(thing.pts.length>1) {
		sX = thing.x + thing.pts[0].x;
		sY = thing.y + thing.pts[0].y;
		ctx.moveTo(sX, sY);
		thing.pts.forEach((e)=>ctx.lineTo(thing.x+e.x,thing.y+e.y));
		// ctx.lineTo(sX,sY);
	}*/
	ctx.save();
	ctx.translate(offsetShakeX+thing.x,offsetShakeY+thing.y);
	ctx.rotate(thing.dir+Math.PI/2);
	ctx.drawImage(thing.img,-thing.w/2,-thing.w/2,thing.w,thing.w);
	ctx.restore();
}



/// LOOPS 


// function titleLoop() {
// 	logicLoop();

// 	writeLine("ACKSTEROI",40,300,180,CENTER);
// 	writeLine("B",50,35,180);
// 	writeLine("D",50,565,180,RIGHT);

// 	// COIL TEST
// 	if(document.monetization && document.monetization.state === 'started' && menuCoil) {
// 		menus.push(menuCoil);
// 		menuCoil = false;
// 	}

// 	// writeLine("0123",40,300,250,CENTER);

// 	if(counts.menu<0) {
// 		if (keys.ArrowUp) menuId = menuId-1+menus.length;
// 		if (keys.ArrowDown) menuId = menuId+1;
// 		menuId %= menus.length;
// 		if(keys.x) {
// 			menus[menuId].fn();
// 		}
// 		keys.ArrowUp = keys.ArrowDown = keys.x = false;
// 	}

// 	menus.forEach((el,id)=>{
// 		writeLine(el.txt,20,70,300+35*id,LEFT,id==menuId);
// 		if(id == menuId) writeLine(">",20,60,300+35*id, RIGHT,true);
// 	})

// }

function gameLoop() {
	if(player && counts.wait<0){
		if(tutoState<3 && txts.length && (keys.ArrowUp|keys.c|keys.x|keys.ArrowLeft|keys.ArrowRight) ) {
			// txts = [];
			esth = [];
		}
		player.left = keys.ArrowLeft;
		player.right = keys.ArrowRight;
		player.up = keys.ArrowUp;
		player.fire = keys.x;
		player.teleport = keys.c;
		keys.ArrowUp = keys.c = keys.x = false;
		if(serialControl) keys.ArrowLeft = keys.ArrowRight = false;
	}

	logicLoop();


	// writeLine(calcScore(player),30,590,40,RIGHT);

	if (player && !player.alive) setEnd();

}

function endGame() {
	startGame();
}

// function localEndLoop() {
// 	logicLoop();

// 	writeLine("GAME OVER",57,300,150,CENTER);
// 	writeLine("YOUR SCORE",30,35,315);
// 	writeLine(player.score,30,565,315,RIGHT);
// 	if (player.score > highscore) {
// 		highscore = player.score;
// 		// player.newHighscore = true;
// 		localStorage.setItem("highscore",highscore);
// 	}
// 	if(player.newHighscore===true) writeLine("HIGH SCORE !",14,565,350,RIGHT,true);

// 	writeLine("X to restart / C to title screen",20,300,580,CENTER);

// 	if(counts.menu<0) {
// 		if(keys.c) setTitle();
// 		if(keys.x) startGame(true);
// 	}
// }


/// TRANSITIONS

// function setTitle() {
// 	if(socket) {
// 		socket.close();
// 		socket = null;
// 	}
// 	errorMsg = null;
// 	player = null;
// 	noGame();

// 	menuId =0;
// 	loop = titleLoop;
// 	counts.menu = momentumMenu;
// }

function startGame() {
	noGame();
	tutoState=0;
	planete=startTime=null;
	
	player = createShip();
	placeShip(player);
	player.x = player.y = width/2;
	player.alive = true;
	ships.push(player);
	keys.x = false;
	tuto();
	loop = gameLoop;
}

function tuto() {
	tutoState ++;
	if (tutoState==1) {
		createAsteroid(width/5,height/3,150,0, 0) 
		txts =[{txt:"Tirez sur cet astéroïd !",x:width/2,y:height/2-rPlanete*4}];
		esth=[{img:arrowImg,x:width*0.29,y:height/2,w:200}];
		counts.wait = momentumWait;
		counts.txt = momentumText;
	} else if (tutoState==2) {
		createAsteroid(width/5*4,height/3,150,0, 0) 
		txts =[{txt:"Foncez sur cet astéroïd !",x:width/2,y:height/2-rPlanete*4}];
		esth=[{img:arrowImg,x:width*0.72,y:height/2,w:200,dir:Math.PI*2}];
		counts.wait = momentumWait;
		counts.txt = momentumText;
	} else if (tutoState==4) {
		planete = createPlanete();
		txts =[{txt:"Protégez la planète !",x:width/2,y:height/2-rPlanete*4}];
		counts.txt = momentumText;
		startTime = Date.now();
	}
}


function setEnd() {
	// loop = localEndLoop;
	// counts.menu = momentumMenu;
}
