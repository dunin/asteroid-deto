const fs = require('fs');
const serialport = require('serialport');
const Readline = require('@serialport/parser-readline');
const { Client } = require('node-osc');

const alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
let imgLoad = 0;
let serialControl=false;
let arrowImg;
// 115200
const serialCommands = {
		L:"ArrowLeft",
		R:"ArrowRight",
		P:"ArrowUp",
		F:"x",
		C:"c"
	}

document.body.onload = ()=>{
	loadCiel();
	astsDispo = loadFolder('res/asteroids/');
	expDispo = loadFolder('res/explosions/');
	planetes = loadFolder('res/planetes/');
	propulsionDispo = loadFolder('res/propulsions/');
    shipImg=loadImg("res/ship.svg");
    arrowImg = loadImg("res/arrow.svg");
}

function loadFolder(path){
	let tab = [];
	let listImg = fs.readdirSync(path);
	listImg = listImg.sort();
	for (var i=0;i<listImg.length;i++) {
        var el = listImg[i];
        if(el.indexOf(".svg")==el.length-4 || el.indexOf(".png")==el.length-4) {
        	tab.push(loadImg(path+el));
        }  
    }
    return tab;
}

function loadImg(path) {
	// var img = document.createElement("img");
	let img = new Image;
    img.src = path;
    imgLoad++;
    img.addEventListener("load",loaded);
    return img;
}

function loaded() {
	imgLoad--;
	if (imgLoad==0) start();
}

function loadCiel() {
	let img = new Image;
    img.src = 'res/ciel/ciel.png'+randomOpt();
    img.addEventListener("load",()=>{
    	cielImg = img;
    	setTimeout(loadCiel,10000); // toutes les 10 secondes
    });
}

function randomOpt() {
	var opt = "?";
	for(var i=0;i<8;i++) {
		opt += alph[rand(0,alph.length)];
	}
	return opt;
}


function start() {
	console.log("start");
	document.addEventListener("keydown",keyDown);
	document.addEventListener("keyup",keyUp);
	getSerial();
	
	setInterval(function(){
		// txts = [];
		frames = [];
		loop();
		draw();
	},frameRate);
	startGame();
}

function getSerial() {
	serialport.list((err, ports) => {
	  if (err) {
	    console.log("serial err",err)
	    return setTimeout(getSerial,1000);
	  }
	  // if(ports.length) console.log('ports', ports);
	  for (var i=0;i<ports.length;i++) {
	  	if (ports[i].manufacturer && ports[i].manufacturer.indexOf("Arduino") != -1) {
	  		return setSerial(ports[i].comName);
	  	}
	  }

	  // last port isn't the good port
	  setTimeout(getSerial,1000);
	  
	});

}

function setSerial(path) {
	const port = new serialport(path, {
	  baudRate: 115200
	});

	port.on('error', function(err) {
	  console.log('Error serial: ', err.message);
	  if (err.disconnected == true) {
	  	getSerial();
	  	serialControl=false;
	  }
	});

	const parser = port.pipe(new Readline({delimiter:"\r\n"}));
	parser.on('data', function (data) {
	  // console.log('Data serial:', JSON.stringify(data),'serialCommands[data]',serialCommands[data]);

	  if(serialCommands[data]) keys[serialCommands[data]] = true;
	});
	serialControl=true;
}

function boom() {
	const client = new Client('192.168.1.206', 9999);
	console.log("send boom");
	client.send('/boom', 1, () => {
	  client.close();
	});
}
