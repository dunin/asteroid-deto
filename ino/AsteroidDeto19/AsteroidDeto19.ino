/*
 * Deto - ClickTournHu
 */

#define REVISION "201909261806"

#define DEBUG 1

// 5v, Gnd to rotary enc (color mess)
#define TOURN_PIN1 2 //a
#define TOURN_PIN2 3 //b

// 5v - 4,7K - capteur pression - analog pin - 1M - Gnd 
#define POMP_PIN1 2  // ANALOG

#define FIRE_ARM_PIN 4 // Gnd- lever - pin
#define FIRE_FIR_PIN 5 // Gnd - Stop Button - pin

#define KEY_PIN 7

int TournPin1Last=LOW;
int TournPin2Last=LOW;
int TournPin1Read=LOW;
int TournPin2Read=LOW;
int TourPosRead =0;
int TourPosLast =0;
int TourArmed = 0;

int K;
uint32_t PompRead = 0;
uint32_t PompVal = 0;
int PonpArmed = 0;

int FireArmLast = LOW;
int FireArmRead = LOW;
int FireArmVal  = 0;
int FireFirLast = LOW;
int FireFirRead = LOW;
int FireFirVal  = 0;
int FirArmed = 0;

char msgs[5] = {
  'L', // Rotation Left
  'R', //Rotation Right
  'F', //Fire
  'P', //Push (Pump)
  'C' }; // Cheat mode
char start_msg[15] = {
  "Start loop "};
int  adc_key_val[5] ={
  30, 150, 360, 535, 760 };
int NUM_KEYS = 5;
int adc_key_in;
int key=-1;
int oldkey=-1;



void setup() {
  pinMode(13, OUTPUT);  //we'll use the debug LED to output a heartbeat
  Serial.begin(115200);

  /* Print that we made it here */
  Serial.println(start_msg);

  pinMode(TOURN_PIN1, INPUT_PULLUP);
  pinMode(TOURN_PIN2, INPUT_PULLUP);
  // no init for analog POMP_PIN1
  pinMode(FIRE_ARM_PIN, INPUT_PULLUP);
  pinMode(FIRE_FIR_PIN, INPUT_PULLUP);
  // no init for analog KEY_PIN
}

void loop()
{
  int Up1 = 0;
  int Up2 = 0;
  int Mov = 0;
  
  adc_key_in = analogRead(KEY_PIN);    // read the value from the sensor
  digitalWrite(13, HIGH);
  /* get the key */
  key = get_key(adc_key_in);    // convert into key press
  if (key != oldkey) {   // if keypress is detected
    delay(50);      // wait for debounce time
    adc_key_in = analogRead(7);    // read the value from the sensor
    key = get_key(adc_key_in);    // convert into key press
    if (key != oldkey) {
      oldkey = key;
      if (key >=0){
        //Serial.println(adc_key_in, DEC);
        Serial.println(msgs[key]);
      }
    }
  }
  digitalWrite(13, LOW);

  TournPin1Read = digitalRead( TOURN_PIN1);
  TournPin2Read = digitalRead( TOURN_PIN2);
  Up1=Up2=0;
  if(TournPin1Last!=TournPin1Read) {
    TournPin1Last = TournPin1Read;
    Up1 = 1;
    //if (DEBUG) Serial.println( "TOURN_PIN1");
  }
  if(TournPin2Last!=TournPin2Read) {
    TournPin2Last=TournPin2Read;
    Up2 = 1;
    TourArmed=1;
    //if (DEBUG) Serial.println( "TOURN_PIN2");
  }
  if (Up1 && TournPin1Last && TourArmed) { // Front 1
    if (TournPin2Last)
      Serial.println( "L");
    else
      Serial.println( "R");
    TourArmed = 0;
  }

  K=50;
  PompVal = analogRead( POMP_PIN1);
  if (PompVal<0)
    PompVal = PompRead;
  PompRead = (PompRead*K+PompVal)/(K+1);
  //if (DEBUG) Serial.println( PompRead);
  if (PompRead > 500) {
     if (!PonpArmed) {
      Serial.println( "P");
     }
     PonpArmed = 1;
  } else if (PompRead < 200) {
    PonpArmed = 0;
  }

  FireArmVal = (FireArmVal*3+digitalRead( FIRE_ARM_PIN)*100)/4;
  FireArmRead = FireArmVal>50;
  if(FireArmLast!=FireArmRead) {
    FireArmLast = FireArmRead;
    //if (DEBUG) Serial.println( "FIRE_ARM_PIN");
    FirArmed = 1;
  }

  FireFirVal = (FireFirVal*3+digitalRead( FIRE_FIR_PIN)*100)/4;
  FireFirRead = FireFirVal>50;
  if(FireFirLast!=FireFirRead) {
    FireFirLast = FireFirRead;
    //if (DEBUG) Serial.println( "FIRE_FIR_PIN");
    if (FirArmed) {
      Serial.println( "F");
      FirArmed = 0;
    }
  }
}

// Convert ADC value to key number
int get_key(unsigned int input)
{
  int theKey=-1;
  for (int k = 0; k < NUM_KEYS; k++)
  {
    if (input < adc_key_val[k])
    {
      theKey = k;
      break;
    }
   
  }
  return theKey;
  /*
  if (k >= NUM_KEYS)
  {
    k = -1;     // No valid key pressed
  return k;
  }
  */
}
