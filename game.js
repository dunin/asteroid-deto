/// VARIABLES

const width=1920;
const height=1080;
const frameRate = 1000/60;
const wAstMin = 60;
const wAstMax = 200;
const astVar = 9;
const astVarAngleMin = Math.PI/7.5;
const astVarAngleMax = Math.PI/7;
const astDirMin = Math.PI/128;
const astDirMax = Math.PI/64;
const astStepsToDir = 300;
const maxVelocity = 3;
const maxVelocityShip = 7;
const expTime = 8;
const expR = 10;
const expVar = 50;
const expVarAngleMin = Math.PI/8;
const expVarAngleMax = Math.PI/2.8;
const rShip = 60;
const rShipColl = 20;
const rotationSpeed = Math.PI/380;
const kickbackSpeed = 2;
const shipSpeed = 0.5;
const momentumFire = 15;
const momentumTeleport = 100;
const momentumAsteroids = 260;
const momentumRevive = 100;
const momentumText = 100;
const momentumWait = 30;
const shakeTime = 10;
const shakeEnd = momentumText;

const rBullet = 2;
const bulletSpeed = 7;
const maxObjects = 60;
const aliveTimePoints = 5000;
const killAstScore = 10;

let astsDispo = [];
let expDispo = [];
let propulsionDispo = [];
let planetes = [];
let asts = [];
let esth = [];
let bulls = [];

let counts={asteroid:0,revive:-1,shake:0,txt:-1,wait:0};

let planete;
const rPlanete = 60;
const lifePlanete = 5;
const propMin = 2;
const propMax = 15;
const propNb = 20;
const propAngleMax = Math.PI/3;
const propTimeMin = 2;
const propTimeMax = 20;
const propMinSpeed = 2;
const propMaxSpeed = 5;

let timeStart = null;


/// UTILITIES

function rand(min, max) {
	return min + Math.floor(Math.random()*(max-min));
}

function getPoint(dir,dist) {
	return {x:Math.cos(dir)*dist, y:Math.sin(dir)*dist}
}

function getVec(dx,dy) {
	return {angle:Math.atan2(dy,dx), dist:Math.sqrt(dx*dx+dy*dy)};
}

function capVel(v,mv) {
	mv = mv? mv:maxVelocity;
	if(v<-mv) return -mv;
	if(v>mv) return mv;
	return v;
}

function setDir(xOry,w) {
	return (xOry - rand(w,width-w)) / astStepsToDir;
}

function checkCollision(o1,o2) {
	if(!o1 || !o2) return false;
	var dist = Math.sqrt( (o2.x-o1.x)*(o2.x-o1.x)+(o2.y-o1.y)*(o2.y-o1.y) );
	return dist < o1.wColl+o2.wColl;
}

function moveObj(o, out) {
	out = true;
	o.x += o.dx;
	o.y += o.dy;
	if (o.x<-o.wColl) o.x = width+o.wColl;
	else if (o.x>o.wColl+width) o.x = -o.wColl;
	else if (o.y<-o.wColl) o.y = height+o.wColl;
	else if (o.y>o.wColl+height) o.y = -o.wColl;
	else out = false;

	if(o.dirV) o.dir+=o.dirV;
	return !out;
}

function countdown (el) {
	if (el.t===undefined) return true;
	return --el.t >1;
}

function getSafePlace() {
	var tries = 100,x,y, wSafe=rShip*3, safe, objs = asts.concat(bulls,planete);
	do {
		if(tries==20) wSafe = rShip;

		x = rand(rShip*2,width-rShip*2);
		y = rand(rShip*2,width-rShip*2);

		safe = !objs.some((o)=>checkCollision(o,{x:x,y:y,w:wSafe}));
		tries--;
	} while(!safe || tries>0);

	return {x:x,y:y};
}



/// ASTEROIDS STUFF

function createAsteroid(x,y,w,dx, dy,r,a) {
	var pts=[];
	w = w? w: rand(wAstMin,wAstMax);
	if(x==undefined) {
		do {
			r = rand(0,4);
			x = r%2? rand(-w,width+w) : (r==0? -w:width+w);
			y = r%2==0? rand(-w,height+w) : (r==1? -w:height+w);
		} while(asts.some((o)=>checkCollision(o,{x:x,y:y,w:w})) && checkCollision(player,{x:x,y:y,w:w}) );
	}

	dx = capVel(dx!=undefined? dx : setDir(x,w) );
	dy = capVel(dy!=undefined? dy : setDir(y,w) );
	let img;
	if(astsDispo.length) img = astsDispo[rand(0,astsDispo.length)];
	else {
		var angle = 0;
		let w2 = w/2;
		var dist;
		// let pt;
		let firstPt=null;
		img = new OffscreenCanvas(w,w);
		img.width = img.height = w;
		let ctxImg = img.getContext("2d");
		ctxImg.strokeStyle = "white";
		ctxImg.fillStyle = "black";
		ctxImg.lineWidth = 2;
		ctxImg.beginPath();

		while (angle<Math.PI*2 - astVarAngleMin) {
			dist = Math.min(w2-1,w2 + rand(-astVar, astVar));
			let pt = getPoint(angle,dist);
			pt.x = Math.round(pt.x+w2);
			pt.y = Math.round(pt.y+w2);
			if(firstPt===null) {
				ctxImg.moveTo(pt.x,pt.y);
				firstPt = pt;
			} else ctxImg.lineTo(pt.x,pt.y);

			angle += rand(astVarAngleMin,astVarAngleMax);
		}
		
		ctxImg.lineTo(firstPt.x,firstPt.y);
		ctxImg.closePath();
		ctxImg.fill();
		ctxImg.stroke();
		
		img = img.transferToImageBitmap();
	}
	asts.push(a={x:x,y:y,dx:dx,dy:dy,w:w,wColl:w/2,dir:0,dirV:astDirMin+Math.random()*(astDirMin-astDirMax),img:img});
	// console.log("createAsteroid",a);
	return a;
}

function killAst(ast,dx,dy) {
	var diffAngle = Math.PI/3;
	if(ast.w/2>wAstMin && tutoState>2) {
		var vec = getVec(dx,dy);
		var nAngle, nDx, nDy, a2,a1,pt;

		nAngle = vec.angle - diffAngle/2 +Math.random()*diffAngle;
		pt = getPoint(nAngle,vec.dist);
		nDx = capVel(ast.dx/2 + pt.x);
		nDy = capVel(ast.dy/2 + pt.y);
		a1 = createAsteroid(ast.x, ast.y, ast.w/2, nDx, nDy) ;

		do {
			nAngle = vec.angle - diffAngle/2 +Math.random()*diffAngle;
			pt = getPoint(nAngle,vec.dist);
			nDx = capVel(ast.dx/2 + pt.x);
			nDy = capVel(ast.dy/2 + pt.y);
			pt = getPoint(nAngle,ast.w);
			a2 = createAsteroid(ast.x+pt.x, ast.y+pt.y, ast.w/2, nDx, nDy);
		} while(checkCollision(a1,a2));

		// asts.push(a1,a2);
		
	}
	asts.splice(asts.indexOf(ast),1);
	if(tutoState<3) tuto();
}


/// EXPLOSIONS STUFF

function createExplosion(o1,o2) {
	var vec, pt, x,y,dist, angle=0, pts=[],count=0,w;
	if(o1.wColl>o2.wColl) {
		var oT = o1;
		o1 = o2;
		o2 = oT;
	}
	var vec = getVec(o2.x-o1.x,o2.y-o1.y);
	var pt = getPoint(vec.angle,o1.wColl);
	x = o1.x + pt.x;
	y = o1.y + pt.y;

	addExplosion(x,y);
}

function addExplosion(x,y,stroke) {
	var imgExp,w;
	if(expDispo.length) {
		imgExp = expDispo[rand(0,expDispo.length)];
		w = imgExp.naturalWidth;
	} else {
		var dist, angle=0, pt,count=0;
		w = expR+expVar;
		var canTemp = new OffscreenCanvas(w, w);
		var ctxTemp = canTemp.getContext("2d");

		ctxTemp.fillStyle = ctxTemp.strokeStyle ="white";
		ctxTemp.lineWidth = 1;
		ctxTemp.beginPath();

		while (angle<Math.PI*2) {
			dist = (count%2)? expR :expR + rand(expR,expVar) ;

			pt = getPoint(angle,dist);
			if(angle==0) ctxTemp.moveTo(w/2+pt.x,w/2+pt.y)
			else ctxTemp.lineTo(w/2+pt.x,w/2+pt.y);

			angle += rand(expVarAngleMin,expVarAngleMax);

			count++;
		}

		if(stroke) ctxTemp.stroke();
		else ctxTemp.fill();
		imgExp = canTemp.transferToImageBitmap();
	}
		
	esth.push({t:expTime,x:x,y:y,img:imgExp,w:w,dir:0});
}


/// SHIPS STUFF

function createShip() {
	return {x:0,y:0,w:rShip,wColl:rShipColl,dir:0,dx:0,dy:0,score:0,highscore:0,alive:false,aliveSince:0,kills:0,counts:{fire:0,teleport:0},img:shipImg};
}

function ptsShip(dir,pts) {
	pts=[];
	for (var i=0;i<3;i++) {
		pts.push(getPoint(dir, rShip*(i==0?2.2:1.6)))
		dir += Math.PI*2/3;
	}
	pts.push(pts[0]);
	return pts;
}

function turnShip(ship,dir) {
	ship.dir += dir;
	ship.pts = ptsShip(ship.dir);
}

function killShip(ship,killer) {
	// id = id? id : ships.indexOf(ship);
	// ships.splice(id,1);
	// ship.score = calcScore(ship);
	
	// if (ship.score>ship.highscore) {
	// 	ship.newHighscore = true;
	// 	ship.highscore = ship.score;
	// }
	// // counts.revive = momentumRevive;
	// ship.alive = false;
	// placeShip(ship);
	counts.shake = shakeTime;
	if(killer){
		ship.dx = capVel((ship.dx + killer.dx)*0.5, maxVelocityShip);
		ship.dy = capVel((ship.dy + killer.dy)*0.5, maxVelocityShip);
	}
}

function placeShip(ship) {
	var coor = getSafePlace();
	ship.x = coor.x;
	ship.y = coor.y;
	ship.dx = ship.dy = 0;
	ship.dir = -Math.PI/2;
	ship.pts = ptsShip(ship.dir);
	ship.alive = true;
	ship.aliveSince = Date.now();
}

function calcScore(ship) {
	if (!ship) return 0;
	return ship.score + (ship.alive? Math.floor((Date.now() - ship.aliveSince)/aliveTimePoints):0);
}

function addPropulsion() {
	for (var i = 0; i < propNb; i++) {
		let imgProp,w = rand(propMin,propMax);
		let a = player.dir+Math.PI-propAngleMax/2+Math.random()*propAngleMax;
		let ptVel = getPoint(a, propMinSpeed+Math.random()*(propMaxSpeed-propMinSpeed));
		let ptStart = getPoint(player.dir+Math.PI, player.w/2);

		if(propulsionDispo.length) {
			imgProp = propulsionDispo[rand(0,propulsionDispo.length)];
		} else {
			var canTemp = new OffscreenCanvas(w, w);
			var ctxTemp = canTemp.getContext("2d");

			ctxTemp.fillStyle = "black";
			ctxTemp.strokeStyle ="white";
			ctxTemp.lineWidth = 1;
			ctxTemp.beginPath();
			ctxTemp.arc(w/2, w/2, w/2, 0, Math.PI*2);
			ctxTemp.fill();
			ctxTemp.stroke();

			imgProp = canTemp.transferToImageBitmap();
		}
			
		esth.push({t:rand(propTimeMin,propTimeMax),x:player.x+ptStart.x,y:player.y+ptStart.y,dx:player.dx+ptVel.x,dy:player.dy+ptVel.y,img:imgProp,w:w,wColl:1000,dir:a});

	}
}


/// BULLETS STUFF

function createBullet(ship) {
	var dXY = getPoint(ship.dir,bulletSpeed);
	var offset = getPoint(ship.dir,rShipColl*2);
	bulls.push({x:ship.x+offset.x,y:ship.y+offset.y,dx:dXY.x,dy:dXY.y,w:1,wColl:1,ship:ship});
}

// PLANET STUFF

function createPlanete() {
	return {x:width/2,y:height/2,w:rPlanete*2,wColl:rPlanete,dir:0,img:planetes[0],life:lifePlanete};
}

function hitPlanete() {
	planete.life--;
	boom();
	if(planete.life==0) {
		counts.shake = shakeEnd;
		txts.push({txt:"Perdu !",x:width/2,y:height/2-rPlanete*2});
		counts.txt = momentumText;
	} else {
		counts.shake = shakeTime;
		planete.img = planetes[planetes.length-Math.floor(planete.life/lifePlanete*planetes.length)];
	}
}


/// GAME LOOP

function logicLoop() {
	var o1,o2/*,deadShips=[]*/;

	// COUNTS
	for (var id in counts) {
	  counts[id]--;
	}

	if(counts.shake>0)return;
	else if(planete && planete.life==0) return endGame();

	if(counts.txt==0) txts = [];


	if(player.alive) {
		player.counts.fire--; player.counts.teleport--;
		if(player.left) turnShip(player,-rotationSpeed);
		if(player.right) turnShip(player,rotationSpeed);
		if(player.up /*&& tutoState!=1*/) {
			var movement = getPoint(player.dir, shipSpeed);
			player.dx = capVel(player.dx + movement.x, maxVelocityShip);
			player.dy = capVel(player.dy + movement.y, maxVelocityShip);
		}
		if(player.fire && (player.counts.fire<1 || serialControl)/* && tutoState!=2*/) {
			// var kickback = getPoint(player.dir+Math.PI, kickbackSpeed);
			// player.dx = capVel(player.dx + kickback.x, maxVelocityShip);
			// player.dy = capVel(player.dy + kickback.y, maxVelocityShip);
			player.counts.fire = momentumFire;
			createBullet(player);
			player.fire = false;
		}
		/*if(player.teleport && player.counts.teleport<1) {
			addExplosion(player.x,player.y,true);
			player.counts.teleport = momentumTeleport;
			player.score = calcScore(player);
			placeShip(player);
			player.teleport = false;
		}*/

		moveObj(player);

		if(checkCollision(player,planete)) {
			hitPlanete();
			createExplosion(player,planete);
			var vec = getVec(planete.x-player.x,planete.y-player.y);
			var pt = getPoint(vec.angle+Math.PI,planete.wColl+player.wColl);
			player.x = planete.x + pt.x;
			player.y = planete.y + pt.y;

			pt = getPoint(vec.angle+Math.PI,shipSpeed*0.75);

			player.dx = pt.x;
			player.dy = pt.y;
		}

		if(player.up){
			player.up = false;
			addPropulsion();
		}
	}

	for (var i=bulls.length-1;i>-1;i--) {
		o1 = bulls[i]
		if (!moveObj(o1)) bulls.splice(i,1);
		else {
			if(player.alive && checkCollision(o1,player)) {
				bulls.splice(i,1);
				// deadShips.push(player);
				killShip(player,o1);
				createExplosion(o1,player);

			} else if(player.alive && checkCollision(o1,planete)) {
				bulls.splice(i,1);
				hitPlanete();
				createExplosion(o1,planete);
			}
		}
	}

	for (var i=asts.length-1;i>-1;i--) {
		o1 = asts[i];
		moveObj(o1);

		// collision between asteroids
		for (var j=i+1;j<asts.length; j++) {
			o2 = asts[j];
			if(checkCollision(o1,o2)) {
				killAst(o1,o2.dx,o2.dy);
				killAst(o2,o1.dx,o1.dy);
				createExplosion(o1,o2);
				break;
			}
		}

		// collision with bullets
		for (var j=0;j<bulls.length; j++) {
			o2 = bulls[j];
			if(checkCollision(o1,o2)) {
				killAst(o1,o2.dx/2,o2.dy/2);
				bulls.splice(j,1);
				createExplosion(o1,o2);
				if(o2.ship.alive) o2.ship.score += killAstScore;
				break;
			}
		}

		// collision with ships
		if(player.alive && checkCollision(o1,player)) {
			killAst(o1,player.dx,player.dy);
			// deadShips.push(player);
			killShip(player,o1);
			createExplosion(o1,player);

		} else if(player.alive && checkCollision(o1,planete)) {
			asts.splice(i,1);
			hitPlanete();
			createExplosion(o1,planete);

		}

	}

	esth = esth.filter(countdown);


	if(tutoState>3 && counts.asteroid<0 && (asts.length<maxObjects)) {
		createAsteroid();
		counts.asteroid = momentumAsteroids;
	}

	// if(counts.revive===0) {
	// 	placeShip(player);
	// 	ships.push(player);
	// }

	// return deadShips;
	if(tutoState==3 && !checkCollision(player,createPlanete()) ) tuto();
}


/// KEY HANDLING

function keyDown(e) {
	keys[e.key] = true;
}

function keyUp(e) {
	keys[e.key] = false;
}
